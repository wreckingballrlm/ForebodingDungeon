The root node, Actor, is a CharacterBody2D.

```mermaid
---
title: Actor Scene
---
classDiagram
    Player "1" --|> "1" Actor: inherits from
    Enemy "1..*" --|> "1" Actor: inherits from

    Actor "1" *-- "1" actorGD: has attached to
    Actor "1" *-- "0..*" Area2D: has
    Actor "1" *-- "1..*" CollisionShape2D: has
    Actor "1" *-- "1" Sprite2D: has
    Actor "1" *-- "1" AnimationPlayer: has
    Actor "1" *-- "1..*" AudioStreamPlayer: has

    actorGD "1" *-- "1" Script: contains
    actorGD "1" *-- "1" ClassName: contains

    Area2D "0..*" *-- "1..*" CollisionShape2D: has own

    CollisionShape2D "1..*" *-- "1" Shape: has

    Sprite2D "1" *-- "1" Texture: has
    Sprite2D "1" *-- "1..*" AnimationFrames: has

    Texture "1" o-- "1" Image: uses

    AnimationFrames "1..*" *-- "1..*" HFrames: has
    AnimationFrames "1..*" *-- "1..*" VFrames: has

    AnimationPlayer "1" o-- "1" Sprite2D: uses
    AnimationPlayer "1" *-- "1..*" Animation: has

    Animation "1..*" o-- "1..*" AnimationFrames: uses

    idle "1" --|> "1" Animation: inherits from
    move "1" --|> "1" Animation: inherits from
    attack "1" --|> "1" Animation: inherits from

    AudioStreamPlayer "1..*" *-- "1" Stream: has

    link Area2D "https://docs.godotengine.org/en/stable/classes/class_area2d.html" _blank
    link CollisionShape2D "https://docs.godotengine.org/en/stable/classes/class_collisionshape2d.html" _blank
    link Sprite2D "https://docs.godotengine.org/en/stable/classes/class_sprite2d.html" _blank
    link AnimationPlayer "https://docs.godotengine.org/en/stable/classes/class_animationplayer.html" _blank
```