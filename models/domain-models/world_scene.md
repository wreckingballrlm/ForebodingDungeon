```mermaid
---
title: World Scene
---
classDiagram
    World "1" *-- "1" worldGD: has attached to
    World "1" o-- "1" TileMap: contains instance of
    World "1" *-- "1" Sprite2D: has
    World "1" *-- "1..*" AudioStreamPlayer: has

    worldGD "1" *-- "1" Script: contains
    worldGD "1" *-- "1" ClassName: contains

    TileMap "1" *-- "1..*" TileSet: has

    AudioStreamPlayer "1" *-- "1" Stream: has

    Stream "1" o-- "1" AudioFile: references
```